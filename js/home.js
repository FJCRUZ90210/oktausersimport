﻿$(document).on('click', '.file-select', function () {
    ConfigInputFile();
});

function ConfigInputFile() {
    $('.file-upload').removeClass('active');
    $('#fileName').text('Cargando...');
    $('#chooseFile').val('');
}

$(document).on('click', '#save', function () {
    
    $('#loading').modal({ show: true, backdrop: 'static', keyboard: false });
    try {
      
        if (ArchivoCargado()) {
            CargaArchivo();                
        }
        else {
            CerrarLoadin();
        }
    }
    catch (e) {
        CerrarLoadin();
        alert(e);
    }
});

function ArchivoCargado() {
    var _OK = false;
    var filePath = $('#chooseFile').val();

    if (filePath !== '') {
        if (filePath.substr(filePath.length - 3, filePath.length) === 'csv') {
            _OK = true;
        } else {
            alert('Archivo con formato incorrecto.');
            ResetFileUpload();
        }
    } else {
        alert('Debe de seleccionar un archivo');
    }

    return _OK
}

function CerrarLoadin() {
    $('#loading').modal('hide');
}

function ResetFileUpload() {
    $('.file-upload').removeClass('active');
    $('#file-select-name').text('Choose a file');
    $('#chooseFile').val = '';
}

function ObtenerArchivo() {
    var obj = new FormData();

    obj.append('file', $('#chooseFile')[0].files[0]);
    obj.append('tipo', $("#TipoActivacion").val());
    obj.append('snPassword', $("#chkPassword").prop("checked"));

    return obj;
}

function ConfigColumna(init) {
    var columnArray = new Array();

    columnArray.push({ "mData": "firstName", "sClass": "text-center" });
    columnArray.push({ "mData": "lastName", "sClass": "text-center" });
    columnArray.push({ "mData": "email", "sClass": "text-center" });
    columnArray.push({ "mData": "login", "sClass": "text-center" });
    columnArray.push({ "mData": "mobilePhone" });
    columnArray.push({ "mData": "password", "sClass": "text-center" });
    columnArray.push({ "mData": "message", "sClass": "text-center" });
    return columnArray;
}

function DatatableEs() {
    var language;
    return language = {
        "decimal": "",
        "emptyTable": "No se encontraron registros.",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "",
        "searchPlaceholder": "Buscar",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": '<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>',
            "previous": '<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>'
        }
    }


}

function CargaArchivo() {
    try {
        var rutaAjax = System.host + 'CargaArchivo/CargarArchivoCSV';

        AjaxCallObject(
            rutaAjax
            , ObtenerArchivo()
            , function functionSuccess(res) {
                ProcesarArchivo();
            }
        );
    }
    catch (e) {
        CerrarLoadin();
        alert(e);
    }
}

function ProcesarArchivo() {
    try {

        var rutaAjax = System.host + 'CargaArchivo/ProcessFile';                

        AjaxCallObject(
            rutaAjax
            , ObtenerArchivo()
            , function functionSuccess(res) {              
                var DatosARR =  res.data;
                $('#table-users').DataTable({
                    "language": DatatableEs(),
                    "data": DatosARR,
                    "dom": 'Bfrtip',
                    "bDestroy": true,
                    "compact": true,
                    "scrollX": true,
                    "aoColumns": ConfigColumna(),
                    "drawCallback": function (settings) {
                        $('[data-toggle="tooltip"]').tooltip();
                    },
                    "initComplete": function (settings, json) {
                        $('#loading').modal('hide');                       
                    }
                });
            }            
        );
        alert('Action Complete.');
    }
    catch (e) {
        alert(e);
    }
}
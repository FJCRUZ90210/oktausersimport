﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration; 

namespace WebAppUsersOktaImport.Helpers
{
    public class Tools
    {
        public static string GetServerHost()
        {
            String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
            String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");

            return strUrl;
        }

        public static String ReadAppSettings(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static String PathServerSubFolder(string folder, string subFolder)
        {
            return HttpContext.Current.Server.MapPath(folder) + @"\\" + subFolder;
        }
    }
}
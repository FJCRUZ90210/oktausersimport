﻿function AjaxCallObject(_url, _data, functionSuccess) {

    $.ajax({
        url: _url,
        data: _data,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        success: function (res) {
            functionSuccess(res)
        },
        //error: function (xhr, status) {
        //    alert('Disculpe, existió un problema');
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Error- Status: " + textStatus + " jqXHR Status: " + jqXHR.status + " jqXHR Response Text:" + jqXHR.responseText)
        },
        complete: function (xhr, status) {            
        }
    });

}
function AjaxCallObjType(_url, _data, functionSuccess, _type) {
    const type = _type.toUpperCase();
    switch (type) {
        case 'POST':
            $.ajax({
                url: _url,
                data: _data,
                type: _type,
                processData: false,
                contentType: false,
                cache: false,
                success: function (res) {
                    functionSuccess(res)
                },
                error: function (xhr, status) {
                    alert('Disculpe, existió un problema');
                },
                complete: function (xhr, status) {
                    //alert('Petición realizada');  Buscar:
                }
            });
            break;
        case 'GET':
            $.ajax({
                url: _url,
                type: 'GET',
                cache: false,
                data: _data ,
                success: function (res) {
                    functionSuccess(res);
                }
            });
            break;
    }

}
function DatatableEs() {
    var language;
    return language = {
                        "decimal": "",
                        "emptyTable": "No se encontraron registros.",
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Mostrar _MENU_ Entradas",
                        "loadingRecords": "Cargando...",
                        "processing": "Procesando...",
                        "search": "",
                        "searchPlaceholder": "Buscar",
                        "zeroRecords": "Sin resultados encontrados",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": '<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>',
                            "previous": '<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>'
                        }
                    }
}
function AgregarOptions(data, opcional) {
    var _length = data.length;
    var option = System.StringEmpty;
    var inicio = 0;

    for (var i = inicio; i < _length; i++) {
        option += i == inicio ? '<option value="' + System.StringEmpty + '"> Seleccione ' + opcional + '</option>' : System.StringEmpty;

        option += '<option value="' + data[i].Item + '">' + data[i].Valor + '</option>'
    }
    return option;
}
function FormatoFechaFiltro(mes, anio) {
    var data = new Object();
    var _mes = parseInt(mes) < 10 ? '0' + mes : mes;
    var ultimo_dia = DeterminarDiaUltimoMes(mes, anio);

    data.FechaInicial = '01/' + _mes + '/' + anio;
    data.FechaFinal= ultimo_dia + '/' + _mes + '/' + anio;

    return '?Fecha_Inicial=' + data.FechaInicial + '&Fecha_Final=' + data.FechaFinal;
}
function FormatoFechaFiltroObj(mes, anio) {
    var data = new Object();
    var _mes = parseInt(mes) < 10 ? '0' + mes : mes;
    var ultimo_dia = DeterminarDiaUltimoMes(mes, anio);

    data.FechaInicial = '01/' + _mes + '/' + anio;
    data.FechaFinal = ultimo_dia + '/' + _mes + '/' + anio;

    return data;
}
function DeterminarDiaUltimoMes(mes, anio) {
    const _mes = parseInt(mes);
    let ultimo_dia;

    switch (_mes) {
        case 1:
            ultimo_dia = '31'
            break;
        case 2:
            ultimo_dia = AnioBisioesto(parseInt(anio));
            break;
        case 3:
            ultimo_dia = '31'
            break;
        case 4:
            ultimo_dia = '30'
            break;
        case 5:
            ultimo_dia = '31'
            break;
        case 6:
            ultimo_dia = '30'
            break;
        case 7:
            ultimo_dia = '31'
            break;
        case 8:
            ultimo_dia = '31'
            break;
        case 9:
            ultimo_dia = '30'
            break;
        case 10:
            ultimo_dia = '31'
        case 11:
            ultimo_dia = '30'
            break;
        case 12:
            ultimo_dia = '31'
            break;
    }
    return ultimo_dia;
}
function AnioBisioesto(initYear) {
    let ultimo_dia;
    if (((initYear % 4 == 0) && (initYear % 100 != 0)) || (initYear % 400 == 0)) {
        ultimo_dia = '29';
    }else {
        ultimo_dia = '28';
    }
    return ultimo_dia;
}
function PintarFilaValida(data, row) {
    if (parseInt(data['VolumenPot']) < parseInt(data['Sep_Reales']) && parseInt(data['ValidoApartir']) < parseInt(data['Sep_Reales'])) {
        $(row).addClass('bonus');
    }
}

$('#chooseFile').bind('change', function () {
    var filename = $("#chooseFile").val();
    $('#fileName').text('');

    if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("Archivo cargado");
        
    }
    else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
        $("#fileName").text("Cargado");
    } 
});
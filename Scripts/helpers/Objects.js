﻿var BonoRCO = ''; //BonoRCO/

var System = {
    host: $('#ctx').val() + BonoRCO,
    hotname: $('#ctx').val(),
    StringEmpty: ''
};
var Icon = {
    Edit: function (data) {
        return '<a class="btn btn-xs btn-success ico-table" data-toggle="tooltip" data-placement="top" title="Editar" data-increment="' + data + '">'
                + '<span class="glyphicon glyphicon-edit pointer" aria-hidden="true"></span></a>';
    },
    Add: function () {
        return '<span class="glyphicon glyphicon-plus pointer" aria-hidden="true"></span>';
    },
    Delete: function (data) {
        return '<a class="btn btn-xs btn-danger ico-table ml-15" data-toggle="tooltip" data-placement="top" title="Eliminar" data-del="' + data + '">'
               +'<span class="glyphicon glyphicon-erase pointer" aria-hidden="true"></span></a>';
    }

};
var ExportExcel = function() {
    return {
        extend: 'excel',
        text: 'Descargar Excel &nbsp;&nbsp;<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
        className: 'exportExcel',
        filename: 'Bonos',
        customize: function (xlsx) {
            var sheet = xlsx.xl.worksheets['sheet1.xml'];

            $('row:first c', sheet).attr('s', '30');
            //color the background of the cells if found the value
            $('row c[r^="O"]', sheet).each(function (indx) {
                $(this).attr('s', '4');
                $(this).attr('s', '12');
            });
            $('row c[r^="P"]', sheet).each(function (indx) {
                $(this).attr('s', '4');
                $(this).attr('s', '12');
            });
            $('row c[r^="J"]', sheet).each(function (indx) {
                $(this).attr('s', '37');
            });
        },
        exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
            modifier: {
                page: 'all'
            }
        }
    };
}
var ExportXlsx = function () {
    return {
        extend: 'excel',
        text: 'Descargar Gral &nbsp;&nbsp;<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
        className: 'exportExcel',
        filename: 'General bonificación'
    };
}

var ExportXlsxCorp = function () {
    return {
        extend: 'excel',
        text: 'Descargar Corp. &nbsp;&nbsp;<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
        className: 'exportExcel',
        filename: 'Coorporativo'
    };
}

var ExportXlsxDec = function () {
    return {
        extend: 'excel',
        text: 'Descargar Dec. &nbsp;&nbsp;<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
        className: 'exportExcel',
        filename: 'Decenal'
    };
}

const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
})
var CruceDetalle = function () {
    return {
        text: 'Detalle de Cruces &nbsp;&nbsp;<i class="fa fa-file-excel-o" aria-hidden="true"></i>',
        className: 'exportExcel',
        action: function (e, dt, node, config) {
            if ($('#mes').val() != System.StringEmpty && $('#anios').val() != System.StringEmpty) {

                var _get = FormatoFechaFiltro($('#mes').val(), $('#anios').val())
                var rutaAjax = System.host + 'CargaArchivo/CrearExcel' + _get;
                window.location.href = rutaAjax;
            } else {
                alert('Debe de seleccionar una fecha');
            }
        }
    }
}
var Http = {
    _POST: 'POST'
    , _GET: 'GET'
    , _DELETE: 'DELETE'
    , _PUT: 'PUT'
    , _PATCH: 'PATCH'
};
﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppUsersOktaImport.Class;
using System.IO;

namespace WebAppUsersOktaImport.Controllers
{
    public class CargaArchivoController : Controller
    {
       
        public ActionResult Home()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CargarArchivoCSV(HttpPostedFileBase file)
        {
            try
            {
                CargarUsuarios.SaveFile(file);

                ModelDefault res1 = new ModelDefault();
                res1.OK = true; 
                return Json(new { data = res1 }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ModelDefault res1 = new ModelDefault();
                res1.OK = false;
                res1.Message = ex.Message;
                return Json(new { data = res1 }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult ProcessFile (HttpPostedFileBase file,string tipo, string snPassword)
        {
            try
            {               
                return Json(new { data = CargarUsuarios.CargarUsuariosOkta(file.FileName,tipo, snPassword) }, JsonRequestBehavior.AllowGet);              
            }
            catch (Exception ex)
            {
                ModelDefault res1 = new ModelDefault();
                res1.Message = ex.Message;
                return Json(new { data = res1 }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
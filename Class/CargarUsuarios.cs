﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Okta.Sdk.Configuration;
using WebAppUsersOktaImport.Class.QuickType;


namespace WebAppUsersOktaImport.Class
{
    public class ModelDefault
    {
        public ModelDefault()
        {
            OK = true;
            Message = String.Empty;
        }
        public bool OK { get; set; }
        public String Message { get; set; }
        public string UrlSingIn { get; set; }
    }

     public class ErrorCaus
    {
        public string errorSummary { get; set; }
    }

    public class OktaError
    {
        public string errorCode { get; set; }
        public string errorSummary { get; set; }
        public string errorLink { get; set; }
        public string errorId { get; set; }
        public List<ErrorCaus> errorCauses { get; set; }
    }

    public class UserModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string login { get; set; }
        public string mobilePhone { get; set; }
        public string password { get; set; }
        public string message { get; set; }
    }
    public class CargarUsuarios
    {

        static List<string> lstFieldsOkta = new List<string>();
        static List<string> lstFieldsOktaRequiered = new List<string>();

        public static void SaveFile(HttpPostedFileBase file)
        {
            string sFile = string.Empty;
            string sPatch = string.Empty;
            string sFullPath = string.Empty;
            string[] sSplit;
            string sPathFriendly = string.Empty;
            string ext = string.Empty;

            sPatch = HttpContext.Current.Server.MapPath("Upload");
            if (!Directory.Exists(sPatch))
            {
                DirectoryInfo di = Directory.CreateDirectory(sPatch);
            }

            sFile = file.FileName;
            sSplit = sFile.Split('\\');
            sFile = sSplit[sSplit.Length - 1];
            sFullPath = sPatch + "\\" + sFile;
            file.SaveAs(sFullPath);
        }
        public static List<UserModel> CargarUsuariosOkta(string file, string Tipo, string snPassword)
        {
            List<UserModel> lst = new List<UserModel>();

            try
            {
                string sFullPath = HttpContext.Current.Server.MapPath("Upload") + "\\" + file;

                lstFieldsOkta = getFieldsProfile();

                using (TextFieldParser csvParser = new TextFieldParser(sFullPath))
                {
                    csvParser.CommentTokens = new string[] { "#" };
                    csvParser.SetDelimiters(new string[] { "," });
                    csvParser.HasFieldsEnclosedInQuotes = true;

                    bool encabezado = true;

                    string[] fields = null;

                    while (!csvParser.EndOfData)
                    {
                        string bodyDinamico = "";
                        string bodyCredentials = "";

                        UserModel user = new UserModel();

                        if (encabezado)
                        {
                            fields = csvParser.ReadFields();
                            encabezado = false;
                            string RetVal = "";
                            RetVal = checkRequieredFields(fields);
                            if (RetVal!="OK")
                            {
                                throw new Exception(RetVal);
                            }
                        }
                        else
                        {
                            string[] values = csvParser.ReadFields();

                            for (int i = 0; i < fields.Count(); i++)
                            {
                                if (esCampoValido(fields[i]))

                                    switch (fields[i])
                                    {
                                        case "firstName":
                                            user.firstName = values[i];
                                            break;
                                        case "lastName":
                                            user.lastName = values[i];
                                            break;
                                        case "email":
                                            user.email = values[i];
                                            break;
                                        case "login":
                                            user.login = values[i];
                                            break;
                                        case "mobilePhone":
                                            user.mobilePhone = values[i];
                                            break;
                                        case "password":
                                            user.password = values[i];
                                            break;
                                    }

                                switch (fields[i])
                                {
                                    case "password":
                                        bodyCredentials = "\"" + "password" + "\"" + ":" + "{" + "\"value\"" +":"+ "\"" + values[i] + "\"" + "}";
                                        user.password = values[0];
                                        break;
                                    case "managerId":
                                        break;
                                    case "manager":
                                        break;
                                    default:
                                        bodyDinamico = bodyDinamico + "\"" + fields[i] + "\"" + ":" + "\"" + values[i] + "\"" + ",";
                                        break;
                                }
                            }

                            bodyDinamico = bodyDinamico.Substring(0, bodyDinamico.Length - 1);
                      
                            user.message = SaveToOkta(user, Tipo, snPassword, bodyDinamico, bodyCredentials);

                            lst.Add(user);
                        }
                    }
                }
                return lst;
            }
            catch (Exception ex)
            {
                UserModel user = new UserModel();
                user.message = "Error ocurred : " + ex.Message ;
                lst.Add(user);
                return lst;
            }
        }

        private static string SaveToOkta(UserModel usr, string Tipo, string snPassword, string bodyDinamicoValues, string bodyCredentialsValues)
        {
            string OktaUrl;

            if (Tipo == "1") //staged
            {
                OktaUrl = "https://dev-225253.okta.com/api/v1/users?activate=false";
            }
            else   //Activated
            {
                OktaUrl = "https://dev-225253.okta.com/api/v1/users?activate=true";
            }

            var client = new RestSharp.RestClient(OktaUrl);
            var request = new RestSharp.RestRequest(RestSharp.Method.POST);


            string message = "User imported with success.";
            try
            {
                request.AddHeader("Accept", "application/json");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", "SSWS 00yg-alor_yDLSgc2ry5DQIB-OvrzqOmRwiwK4i_LR");

                if (snPassword == "false") //Sin Password
                {
                    string profile = "{" + "\"" + "profile" + "\"" + ": {";
                    profile = profile + bodyDinamicoValues + "} }";
                    request.AddJsonBody(profile);
                }
                else //Con password
                {    
                    string body = "{" + "\"" + "profile" + "\"" + ": {";
                    body = body + bodyDinamicoValues + "} ,";
                    body = body + "\"" + "credentials" + "\"" + ": {" + bodyCredentialsValues + " }";
                    body = body + "}}";

                    request.AddJsonBody(body);
                }

                var response = client.Execute(request);

                OktaError error;
                error = Newtonsoft.Json.JsonConvert.DeserializeObject<OktaError>(response.Content);

                if (error.errorCode != null)
                {
                    message = "Error: " + error.errorSummary + " Cause: ";
                    if (error.errorCauses != null && error.errorCauses.Count!=0)
                    {
                        message += error.errorCauses[0].errorSummary;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return message;
        }

        private static bool esCampoValido(string campo)
        {
            bool campoValido = false;

            var match = lstFieldsOkta.FirstOrDefault(stringToCheck => stringToCheck.Contains(campo));

            if (match != null)
                campoValido = true;

            return campoValido;
        }

        private static string checkRequieredFields(string[] fields)
        {
            string retVal = "The following required data were not provided: ";
            bool snFaltanCampos = false; 

            for(int i=0; i< lstFieldsOktaRequiered.Count();i++)
            {
                var match = fields.Contains(lstFieldsOktaRequiered[i]);
                if (match == false)
                {
                    retVal = retVal + lstFieldsOktaRequiered[i] + " " ;
                    snFaltanCampos = true;
                }
            }
            if (snFaltanCampos == true)
                return retVal;
            else
                return "OK";
        }
        
        private static List<string> getFieldsProfile()
        {
            List<string> lst = new List<string>();
            string OktaUrl = "https://dev-225253.okta.com/api/v1/meta/schemas/user/default";  

            var client = new RestSharp.RestClient(OktaUrl);
            var request = new RestSharp.RestRequest(RestSharp.Method.GET);

            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "SSWS 00yg-alor_yDLSgc2ry5DQIB-OvrzqOmRwiwK4i_LR");

            var response = client.Execute(request);
            
            Profile profile = Profile.FromJson(response.Content.ToString());
            
            foreach (KeyValuePair<string, Property> item in profile.Definitions.Base.Properties)
            {
                lst.Add(item.Key);
            }

            foreach (KeyValuePair<string, Property> item in profile.Definitions.Custom.Properties)
            {
                lst.Add(item.Key);
            }

            for (int i=0;i < profile.Definitions.Base.BaseRequired.Count();i++)
            {
                lstFieldsOktaRequiered.Add(profile.Definitions.Base.BaseRequired[i]);
            }
                      
            return lst; 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppUsersOktaImport.Class
{
    namespace QuickType
    {
        using System;
        using System.Collections.Generic;

        using System.Globalization;
        using Newtonsoft.Json;
        using Newtonsoft.Json.Converters;

        public partial class Profile
        {
            [JsonProperty("id")]
            public Uri Id { get; set; }

            [JsonProperty("$schema")]
            public Uri Schema { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("title")]
            public string Title { get; set; }

            [JsonProperty("description")]
            public string Description { get; set; }

            [JsonProperty("lastUpdated")]
            public DateTimeOffset LastUpdated { get; set; }

            [JsonProperty("created")]
            public DateTimeOffset Created { get; set; }

            [JsonProperty("definitions")]
            public Definitions Definitions { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("properties")]
            public ProfileProperties Properties { get; set; }

            [JsonProperty("_links")]
            public Links Links { get; set; }
        }

        public partial class Definitions
        {
            [JsonProperty("custom")]
            public Custom Custom { get; set; }

            [JsonProperty("base")]
            public Base Base { get; set; }
        }

        public partial class Base
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("properties")]
            public Dictionary<string, Property> Properties { get; set; }

            [JsonProperty("required")]
            public string[] BaseRequired { get; set; }
        }

        public partial class Property
        {
            [JsonProperty("title")]
            public string Title { get; set; }

            [JsonProperty("type")]
            public TypeEnum Type { get; set; }

            [JsonProperty("mutability")]
            public Mutability Mutability { get; set; }

            [JsonProperty("scope")]
            public Scope Scope { get; set; }

            [JsonProperty("permissions")]
            public Permission[] Permissions { get; set; }

            [JsonProperty("format", NullValueHandling = NullValueHandling.Ignore)]
            public string Format { get; set; }

            [JsonProperty("required", NullValueHandling = NullValueHandling.Ignore)]
            public bool? PropertyRequired { get; set; }

            [JsonProperty("minLength", NullValueHandling = NullValueHandling.Ignore)]
            public long? MinLength { get; set; }

            [JsonProperty("maxLength", NullValueHandling = NullValueHandling.Ignore)]
            public long? MaxLength { get; set; }
        }

        public partial class Permission
        {
            [JsonProperty("principal")]
            public Principal Principal { get; set; }

            [JsonProperty("action")]
            public Mutability Action { get; set; }
        }

        public partial class Custom
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("properties")]
            public Dictionary<string, Property> Properties { get; set; }

            [JsonProperty("required")]
            public string[] BaseRequired { get; set; }
        }

        public partial class CustomProperties
        {
        }

        public partial class Links
        {
            [JsonProperty("self")]
            public Self Self { get; set; }

            [JsonProperty("type")]
            public Self Type { get; set; }
        }

        public partial class Self
        {
            [JsonProperty("rel")]
            public string Rel { get; set; }

            [JsonProperty("href")]
            public Uri Href { get; set; }

            [JsonProperty("method")]
            public string Method { get; set; }
        }

        public partial class ProfileProperties
        {
            [JsonProperty("profile")]
            public ProfileClass Profile { get; set; }
        }

        public partial class ProfileClass
        {
            [JsonProperty("allOf")]
            public AllOf[] AllOf { get; set; }
        }

        public partial class AllOf
        {
            [JsonProperty("$ref")]
            public string Ref { get; set; }
        }

        public enum Mutability { Hide, ReadOnly, ReadWrite };

        public enum Principal { Self };

        public enum Scope { None };

        public enum TypeEnum { String };

        public partial class Profile
        {
            public static Profile FromJson(string json) => JsonConvert.DeserializeObject<Profile>(json, QuickType.Converter.Settings);
        }

        public static class Serialize
        {
            public static string ToJson(this Profile self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
        }

        internal static class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Converters =
            {
                MutabilityConverter.Singleton,
                PrincipalConverter.Singleton,
                ScopeConverter.Singleton,
                TypeEnumConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
            };
        }

        internal class MutabilityConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Mutability) || t == typeof(Mutability?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "HIDE":
                        return Mutability.Hide;
                    case "READ_ONLY":
                        return Mutability.ReadOnly;
                    case "READ_WRITE":
                        return Mutability.ReadWrite;
                }
                throw new Exception("Cannot unmarshal type Mutability");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Mutability)untypedValue;
                switch (value)
                {
                    case Mutability.Hide:
                        serializer.Serialize(writer, "HIDE");
                        return;
                    case Mutability.ReadOnly:
                        serializer.Serialize(writer, "READ_ONLY");
                        return;
                    case Mutability.ReadWrite:
                        serializer.Serialize(writer, "READ_WRITE");
                        return;
                }
                throw new Exception("Cannot marshal type Mutability");
            }

            public static readonly MutabilityConverter Singleton = new MutabilityConverter();
        }

        internal class PrincipalConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Principal) || t == typeof(Principal?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "SELF")
                {
                    return Principal.Self;
                }
                throw new Exception("Cannot unmarshal type Principal");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Principal)untypedValue;
                if (value == Principal.Self)
                {
                    serializer.Serialize(writer, "SELF");
                    return;
                }
                throw new Exception("Cannot marshal type Principal");
            }

            public static readonly PrincipalConverter Singleton = new PrincipalConverter();
        }

        internal class ScopeConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Scope) || t == typeof(Scope?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "NONE")
                {
                    return Scope.None;
                }
                throw new Exception("Cannot unmarshal type Scope");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Scope)untypedValue;
                if (value == Scope.None)
                {
                    serializer.Serialize(writer, "NONE");
                    return;
                }
                throw new Exception("Cannot marshal type Scope");
            }

            public static readonly ScopeConverter Singleton = new ScopeConverter();
        }

        internal class TypeEnumConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(TypeEnum) || t == typeof(TypeEnum?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "string")
                {
                    return TypeEnum.String;
                }
                throw new Exception("Cannot unmarshal type TypeEnum");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (TypeEnum)untypedValue;
                if (value == TypeEnum.String)
                {
                    serializer.Serialize(writer, "string");
                    return;
                }
                throw new Exception("Cannot marshal type TypeEnum");
            }

            public static readonly TypeEnumConverter Singleton = new TypeEnumConverter();
        }
    }

}